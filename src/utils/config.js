const APIV1 = '/transfer-express-services/api'
const APIV2 = '/api/v2'

module.exports = {
  name: '亚马逊收款管理',
  prefix: 'easypayxAdmin',
  footerText: 'EasyPay Design Admin  © 2017',
  logo: '/logo.png',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',
  CORS: [],
  openPages: ['/login'],
  apiPrefix: '/api',
  APIV1,
  APIV2,
  api: {
    userLogin: `/transfer-express-services/login`,
    userLogout: `${APIV1}/user/logout`,
    userInfo: `${APIV1}/userInfo`,
    users: `${APIV1}/admin/users`,
    user: `${APIV1}/users/:id/view`,
    userDetail: `${APIV1}/aiyou/users/:id/detail`,
    menus: `${APIV1}/menus`,
    weather: `${APIV1}/weather`,
    v1test: `${APIV1}/test`,
    v2test: `${APIV2}/test`,
    updateUserFeeRate: `${APIV1}/admin/user-fee-rates`,
  },
}
