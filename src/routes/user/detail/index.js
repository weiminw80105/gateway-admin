import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import styles from './index.less';
import { Card, Col, Row, Spin, Table, Tag, Button, Modal } from 'antd';
import FeeRateModal from '../FeeRateModal'


const Detail = ({ location, dispatch, userDetail, loading }) => {
  const { currentItem, modalVisible, user } = userDetail
  // console.log(loading)
  const pageLoading = loading.effects['userDetail/query']
  const onEditItem = (record) => {
    dispatch({
      type: 'userDetail/showModal',
      payload: {
        modalType: 'update',
        currentItem: record,
      },
    })
  }

  const modalProps = {
    item: currentItem,
    visible: modalVisible,
    onOk (data) {
      dispatch({
        type: 'userDetail/updateFeeRate',
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'userDetail/hideModal',
      })
    },
  }

  const transColumns = [{
    title: '#',
    dataIndex: 'transactionKey',

  }, {
    title: '时间',
    dataIndex: 'transactionTime',
  }, {
    title: '账号',
    dataIndex: 'accountNumber',
  }, {
    title: '金额',
    dataIndex: 'amount',
    key: 'amount',
    render: (text, record) => {
      if (record.transactionType === 2) {
        return (
          <span style={{ color: 'red' }}> - ${text} </span>
        )
      }
      return (
        <span style={{ color: 'green' }}> + ${text} </span>
      )
    },
  }, {
    title: '费用',
    dataIndex: 'fee',
    render: (text) => <span>${text}</span>
  }, {
    title: '状态',
    dataIndex: 'status',
    key:"status",
    render: (text,record) => {
      if(text===18){
        return <Tag color="green">入账完成</Tag>
      } else if(text === 16) {
        return <Tag color="green">银行入账中</Tag>
      } else if(text === 32) {
        return <Tag color="red">提现处理中</Tag>
      } else if(text === 34){
        return <Tag color="red">提现完毕</Tag>
      } else {
        return <Tag>未知状态</Tag>
      }
    }
  }]

  const feeRateColumns = [{
    title: '',
    dataIndex: 'currency',
    key: 'currency',
    render: (text) => {
      if (text === 1) {
        return (
          '人民币'
        )
      } else if (text === 2) {
        return '美元'
      }
      return '未知'
    },
  }, {
    title: '',
    dataIndex: 'feeRate',
    key: 'feeRate',
    render: (text, record) => {
      if (record.feeType === 2) {
        return (
          <Tag color="red">{text}</Tag>
        )
      }
      return (
        <Tag color="green">{text}</Tag>
      )
    },
  }, {
    title: '',
    render: (record) => {
      return (<Button onClick={onEditItem.bind(null,record)}>修改</Button>)
    },
  }]




  return (<div>
    <div >
      <FeeRateModal title="修改手续费" {...modalProps} />
      <Row gutter={32}>

        <Col span="14">
          <Card bordered={false}>
            <Table
              loading={pageLoading}
              columns={transColumns}
              dataSource={(userDetail.data.data) ? userDetail.data.data.userTransactions : []}
              rowKey={record => record.transactionKey}
            >
            </Table>
          </Card>
        </Col>

        <Col span="10">
          <Row>
            <Col>
              <Card bordered={false} title="手续费">
                <Table
                  loading={pageLoading}
                  dataSource={(!pageLoading) ? userDetail.data.data.feeRates : []}
                  pagination={false}
                  showHeader={false}
                  columns={feeRateColumns}
                  rowKey={(record, key) => key}></Table>
              </Card>
            </Col>
          </Row>

        </Col>
      </Row>
    </div>
  </div>)
}

Detail.propTypes = {
  modalVisible: PropTypes.bool,
  userDetail: PropTypes.object,
}

export default connect(({ userDetail, loading }) => ({userDetail, loading }))(Detail)
