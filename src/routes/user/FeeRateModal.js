import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Modal, InputNumber, Select } from 'antd'

const FormItem = Form.Item
const Option = Select.Option
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const FeeRateModal = ({ item = {}, onOk, form: { getFieldDecorator, validateFields, getFieldsValue, getFieldValue }, ...modalProps }) => {
  const handleOk = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      // const data = {
      //   ...getFieldsValue(),
      //   key: item.key,
      // }
      const data = Object.assign({}, item, {
        feeRate: getFieldValue('feeRate'),
      })
      onOk(data)
    })
  }

  const modalOpts = {
    ...modalProps,
    onOk: handleOk,
  }

  return (
    <Modal {...modalOpts}>
      <Form layout="horizontal">
        <FormItem label="货币" hasFeedback {...formItemLayout}>
          {getFieldDecorator('currency', {
            initialValue: ""+item.currency,
            rules: [
              {
                required: true,
              },
            ],
          })(<Select disabled>
            <Option value={"1"}>人民币</Option>
            <Option value={"2"}>美元</Option>
            <Option value={"3"}>港币</Option>
          </Select>)}
        </FormItem>
        <FormItem label="费率" hasFeedback {...formItemLayout}>
          {getFieldDecorator('feeRate', {
            initialValue: item.feeRate,
            rules: [
              {
                required: true,
              },
            ],
          })(<InputNumber min={0.0035} max={0.012} step={0.0005} />)}
        </FormItem>

      </Form>
    </Modal>
  )
}

FeeRateModal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default Form.create()(FeeRateModal)
