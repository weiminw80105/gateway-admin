import { request, config } from 'utils'
import QuryString from 'query-string'
const { api } = config
const { users } = api

export async function query (params) {
  console.log(params)
  const auth = sessionStorage.getItem("auth");
  return request({
    url: users,
    method: 'get',
    headers: {
      Authorization: auth,
      "Content-Type": 'application/json',
    },
    params,
  })
}

export async function remove (params) {
  return request({
    url: users,
    method: 'delete',
    data: params,
  })
}
