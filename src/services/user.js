import { request, config } from 'utils'

const { api } = config
const { updateUserFeeRate, userDetail } = api

export async function query (params) {
  console.log(params);
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: userDetail.replace(':id',params.id),
    method: 'get',
    headers: {
      Authorization: auth,
    },
  })
}

export async function create (params) {
  return request({
    url: user.replace('/:id', ''),
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: user,
    method: 'delete',
    data: params,
  })
}

export async function update (params) {
  return request({
    url: user,
    method: 'patch',
    data: params,
  })
}

export async function updateFeeRate(params) {
  const auth = sessionStorage.getItem("auth")
  return request({
    url: updateUserFeeRate,
    method: 'post',
    data: params,
    headers: {
      Authorization: auth,
    },
  })
}
