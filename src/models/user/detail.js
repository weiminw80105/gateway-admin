import modelExtend from 'dva-model-extend'
import pathToRegexp from 'path-to-regexp'
import { query, updateFeeRate } from '../../services/user'
import { pageModel } from '../common'

export default modelExtend(pageModel,{
  namespace: 'userDetail',
  state: {
    data: {},
    modalVisible: false,
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen(({ pathname }) => {
        const match = pathToRegexp('/user/:id').exec(pathname)
        console.log(match)
        console.log(pathname)
        if (match) {
          dispatch({ type: 'query', payload: { id: match[1] } })
        }
      })
    },
  },

  effects: {
    * query ({
      payload,
    }, { call, put }) {
      const data = yield call(query, payload)
      const { success, message, status, ...other } = data
      console.log(data)
      if (success) {
        yield put({
          type: 'querySuccess',
          payload: {
            data: other,
          },
        })
      } else {
        throw data
      }
    },
    * updateFeeRate ({ payload }, { call, put }) {
      const data = yield call(updateFeeRate, payload)

      const { success, message, status, ...other } = data
      if (success) {
        yield put({ type: 'hideModal' })
        yield put({ type: 'query', payload: { id: payload.userId } })
      } else {
        throw data
      }
    },
  },

  reducers: {
    querySuccess (state, { payload }) {
      const { data } = payload
      return {
        ...state,
        data,
      }
    },

    showModal (state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal (state, { payload }) {
      return { ...state, ...payload, modalVisible: false }
    },
  },
})
